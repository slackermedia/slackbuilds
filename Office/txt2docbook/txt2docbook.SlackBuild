#!/bin/sh

# Slackware build script for txt2docbook

# Copyright 2012 klaatu klaatu@member.fsf.org
# GNU All-Permissive License
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

PRGNAM=txt2docbook
VERSION=${VERSION:-0.91}
BUILD=${BUILD:-1}
TAG=${TAG:-_SMi}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i486 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

CWD=$(pwd)
TMP=${TMP:-/tmp/SBo}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

if [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e 

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $PRGNAM-$VERSION
mkdir $TMP/$PRGNAM-$VERSION
unzip $CWD/$PRGNAM-$VERSION.zip -d $TMP/$PRGNAM-$VERSION
cd $PRGNAM-$VERSION
chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

mkdir -p $PKG/usr/bin/
cp txt2docbook.pl $PKG/usr/bin/
chmod 755 $PKG/usr/bin/txt2docbook.pl

mkdir -p $PKG/usr/local/share/perl5/
cp output*pl $PKG/usr/local/share/perl5/
cat output.pl > $PKG/usr/local/share/perl5/output_chapter.pl
cp blocks.pm $PKG/usr/local/share/perl5/

patch $PKG/usr/bin/txt2docbook.pl < $CWD/txt2docbook.patch
patch $PKG/usr/local/share/perl5/output_chapter.pl < $CWD/chapter.patch
sed -i 's%=\"output.pl\";%=\"/usr/local/share/perl5/output.pl\";%' $PKG/usr/bin/txt2docbook.pl

find $PKG -depth -type d -empty -delete || true

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a \
  doc.??? README LICENSE.txt \
    todo.xml todo.html todo.txt \
    $CWD/txt2docbook.patch \
  $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tgz}
